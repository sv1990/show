#ifdef NDEBUG
#  undef NDEBUG
#endif

#include "show/assert.hh"

#include <benchmark/benchmark.h>

#include <cassert>

const int n = 1e3;

static void base_bench(benchmark::State& state) {
  for (auto _ : state) {
    int acc = 0;
    for (int i = 0; i < n; ++i) {
      assert(i < n);
      benchmark::DoNotOptimize(acc += i);
    }
  }
}

BENCHMARK(base_bench);

static void assert_bench(benchmark::State& state) {
  for (auto _ : state) {
    int acc = 0;
    for (int i = 0; i < n; ++i) {
      ASSERT_LE(i, n);
      benchmark::DoNotOptimize(acc += i);
    }
  }
}
BENCHMARK(assert_bench);


BENCHMARK_MAIN();
