#define SHOW_RANGE_LENGTH_LIMIT (-1)
#include "show/show.hh"

#include <benchmark/benchmark.h>

#include <numeric>
#include <ostream>
#include <sstream>
#include <vector>

const std::size_t N = 1e2;

template <typename T>
std::ostream& print_to_stream(std::ostream& os,
                              const std::vector<std::vector<T>>& vec) {
  os << '[';
  for (std::size_t i = 0; i < vec.size(); ++i) {
    if (i != 0) {
      os << ", ";
    }
    os << '[';
    for (std::size_t j = 0; j < vec.size(); ++j) {
      os << (j == 0 ? "" : ", ") << vec[i][j];
    }
    os << ']';
  }
  os << ']';
  return os;
}

static void base_bench(benchmark::State& state) {
  std::vector<int> v(N);
  std::iota(begin(v), end(v), 1);
  std::vector<std::vector<int>> vv(N, v);
  std::ostringstream oss;
  for (auto _ : state) {
    benchmark::DoNotOptimize(print_to_stream(oss, vv));
  }
}
BENCHMARK(base_bench);

static void show_bench(benchmark::State& state) {
  std::vector<int> v(N);
  std::iota(begin(v), end(v), 1);
  std::vector<std::vector<int>> vv(N, v);
  std::ostringstream oss;
  for (auto _ : state) {
    benchmark::DoNotOptimize(oss << show::show(vv));
  }
}
BENCHMARK(show_bench);

BENCHMARK_MAIN();
