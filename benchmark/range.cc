#define SHOW_RANGE_LENGTH_LIMIT (-1)
#include "show/show.hh"

#include <benchmark/benchmark.h>

#include <numeric>
#include <ostream>
#include <sstream>
#include <vector>

const std::size_t N = 1e2;

template <typename T>
std::ostream& print_to_stream(std::ostream& os, const std::vector<T>& vec) {
  os << '[';
  for (std::size_t i = 0; i < vec.size(); ++i) {
    os << (i == 0 ? "" : ", ") << vec[i];
  }
  os << ']';
  return os;
}

static void base_bench(benchmark::State& state) {
  std::vector<int> v(N);
  std::iota(begin(v), end(v), 1);
  for (auto _ : state) {
    std::ostringstream oss;
    benchmark::DoNotOptimize(print_to_stream(oss, v));
  }
}
BENCHMARK(base_bench);

static void show_bench(benchmark::State& state) {
  std::vector<int> v(N);
  std::iota(begin(v), end(v), 1);
  for (auto _ : state) {
    std::ostringstream oss;
    benchmark::DoNotOptimize(oss << show::show(v));
  }
}
BENCHMARK(show_bench);

BENCHMARK_MAIN();
