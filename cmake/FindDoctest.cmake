# - Find Doctest
# Find the native Doctest includes and library
#
#  Doctest_INCLUDES    - where to find doctest.h
#  Doctest_FOUND       - True if Doctest found.

if (Doctest_INCLUDES)
  # Already in cache, be silent
  set (Doctest_FIND_QUIETLY TRUE)
endif (Doctest_INCLUDES)

find_path (Doctest_INCLUDES doctest.h)

# handle the QUIETLY and REQUIRED arguments and set Doctest_FOUND to TRUE if
# all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (Doctest DEFAULT_MSG Doctest_INCLUDES)

mark_as_advanced (Doctest_INCLUDES)
