#ifndef SHOW_INCLUDE_SHOW_ASSERT_HH_1530868900551122724_
#define SHOW_INCLUDE_SHOW_ASSERT_HH_1530868900551122724_

#include "show.hh"

#include <iostream>
#include <string>
#include <type_traits>
#include <utility>

#include <cmath>

namespace show { namespace detail {
template <typename F, typename... Ts>
void pack_for_each(F&& f, Ts&&... xs) {
  (f(std::forward<Ts>(xs)), ...);
}

template <typename... Ts>
std::string concat(const Ts&... xs) {
  std::ostringstream oss;
  pack_for_each([&oss](auto&& x) mutable { oss << x; }, xs...);
  return std::move(oss).str();
}

template <typename... Ts>
void print_args(Ts&&... xs) noexcept {
  pack_for_each([](auto&& x) { std::cerr << x; }, std::forward<Ts>(xs)...);
}

template <typename T>
std::string assert_print(const char* name, const T& val) noexcept {
  return concat(name, " = ", to_string(val), "\n");
}

template <typename T>
std::string assert_print(const char* name, const T&& val) noexcept {
  const auto val_str = to_string(val);
  if (val_str == name) {
    return "";
  }
  else {
    return concat(name, " = ", val_str, "\n");
  }
}

template <typename... Ts>
void assertion_failed(const char* expr, const char* file, int line,
                      const char* func, Ts&&... xs) noexcept {
  std::cerr << file << ':' << line << ": " << func << ":\n"
            << "Assertion '" << expr << "' failed with\n";
  print_args(std::forward<Ts>(xs)...);
#ifndef SHOW_ASSERT_NOABORT
  std::abort();
#endif
}

template <typename T>
constexpr bool in_range_impl(const T& x, const T& min, const T& max) {
  return x >= min && x < max;
}

template <typename T1, typename T2, typename T3>
constexpr bool in_range(const T1& x, const T2& min, const T3& max) {
  return in_range_impl<std::common_type_t<T1, T2, T3>>(x, min, max);
}

#ifndef SHOW_FLOAT_EQUAL_EPS
#  define SHOW_FLOAT_EQUAL_EPS 1e-3
#endif

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(std::is_floating_point<T>::value)
constexpr bool equal_impl(const T& x, const T& y) noexcept {
  constexpr T eps{SHOW_FLOAT_EQUAL_EPS};
  // clang-format off
  if constexpr(eps > 0) {
    return std::abs(x - y) < eps;
  }
  else {
    return x == y;
  }
  // clang-format on
}

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(!std::is_floating_point<T>::value)
constexpr bool equal_impl(const T& x, const T& y) noexcept {
  return x == y;
}

template <typename T1, typename T2>
constexpr bool equal(const T1& x, const T2& y) noexcept {
  return equal_impl<typename std::common_type_t<T1, T2>>(x, y);
}

}} // namespace show::detail

#define SHOW_ASSERT_PRINT(x) show::detail::assert_print(#x, x)

#ifndef NDEBUG
#  define SHOW_ASSERT_IMPL(expr, rep, ...)                                     \
    ((expr) ? (void)0                                                          \
            : show::detail::assertion_failed(                                  \
                rep, __FILE__, __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__))
#else
#  define SHOW_ASSERT_IMPL(expr, ...) (void)0
#endif

#define SHOW_ASSERT(expr, ...) SHOW_ASSERT_IMPL(expr, #expr, ##__VA_ARGS__)

#define SHOW_ASSERT_EQ(x, y)                                                   \
  SHOW_ASSERT_IMPL(show::detail::equal(x, y), (#x " == " #y),                  \
                   SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_NEQ(x, y)                                                  \
  SHOW_ASSERT_IMPL(!show::detail::equal(x, y), (#x " != " #y),                 \
                   SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_GE(x, y)                                                   \
  SHOW_ASSERT(x > y, SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_GEQ(x, y)                                                  \
  SHOW_ASSERT(x >= y, SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_LE(x, y)                                                   \
  SHOW_ASSERT(x < y, SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_LEQ(x, y)                                                  \
  SHOW_ASSERT(x <= y, SHOW_ASSERT_PRINT(x), SHOW_ASSERT_PRINT(y))

#define SHOW_ASSERT_IN_RANGE(x, min, max)                                      \
  SHOW_ASSERT_IMPL(show::detail::in_range(x, min, max),                        \
                   (#min " <= " #x " < " #max), SHOW_ASSERT_PRINT(x),          \
                   SHOW_ASSERT_PRINT(min), SHOW_ASSERT_PRINT(max))

#ifndef SHOW_DISABLE_SHORT_ASSERTS
#  define ASSERT          SHOW_ASSERT
#  define ASSERT_EQ       SHOW_ASSERT_EQ
#  define ASSERT_NEQ      SHOW_ASSERT_NEQ
#  define ASSERT_GE       SHOW_ASSERT_GE
#  define ASSERT_GEQ      SHOW_ASSERT_GEQ
#  define ASSERT_LE       SHOW_ASSERT_LE
#  define ASSERT_LEQ      SHOW_ASSERT_LEQ
#  define ASSERT_IN_RANGE SHOW_ASSERT_IN_RANGE
#endif

#endif // SHOW_INCLUDE_SHOW_ASSERT_HH_1530868900551122724_
