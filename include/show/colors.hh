#ifndef SHOW_INCLUDE_SHOW_COLORS_HH_1586076787368070108_
#define SHOW_INCLUDE_SHOW_COLORS_HH_1586076787368070108_

#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>

#include <stdio.h>
#include <unistd.h>

// http://misc.flogisoft.com/bash/tip_colors_and_formatting

namespace show { namespace colors {

namespace detail {
// https://stackoverflow.com/a/12827233/4990485
[[nodiscard]] inline bool file_supports_colors(FILE* f) noexcept {
  return isatty(fileno(f));
}

[[nodiscard]] inline bool stream_supports_colors(std::ostream& os) noexcept {
  return (&os == &std::cout && file_supports_colors(stdout))
         || (((&os == &std::clog) || &os == &std::cerr)
             && file_supports_colors(stderr));
}
} // namespace detail

struct color {
  const char* code;
  constexpr explicit color(const char* code_) noexcept : code(code_) {}
  color(const color&) = delete;
};

inline std::ostream& operator<<(std::ostream& os, const color& c) {
  if (detail::stream_supports_colors(os)) {
    return os << c.code;
  }
  else {
    return os;
  }
}

// Reduce boilerplate code
#define MAKE_COLOR(name, code) inline constexpr color name("\033[" #code "m")

MAKE_COLOR(reset_all, 0);
MAKE_COLOR(bold, 1);
MAKE_COLOR(dim, 2);
MAKE_COLOR(underlined, 4);
MAKE_COLOR(blink, 5);
MAKE_COLOR(reverse, 7);
MAKE_COLOR(hidden, 8);

MAKE_COLOR(reset_bold, 21);
MAKE_COLOR(reset_dim, 22);
MAKE_COLOR(reset_underlined, 24);
MAKE_COLOR(reset_blink, 25);
MAKE_COLOR(reset_reverse, 27);
MAKE_COLOR(reset_hidden, 28);

MAKE_COLOR(black, 30);
MAKE_COLOR(red, 31);
MAKE_COLOR(green, 32);
MAKE_COLOR(yellow, 33);
MAKE_COLOR(blue, 34);
MAKE_COLOR(magenta, 35);
MAKE_COLOR(cyan, 36);
MAKE_COLOR(light_gray, 37);
MAKE_COLOR(default_, 39);
MAKE_COLOR(dark_gray, 90);
MAKE_COLOR(light_red, 91);
MAKE_COLOR(light_green, 92);
MAKE_COLOR(light_yellow, 93);
MAKE_COLOR(light_blue, 94);
MAKE_COLOR(light_magenta, 95);
MAKE_COLOR(light_cyan, 96);
MAKE_COLOR(white, 97);

MAKE_COLOR(background_black, 40);
MAKE_COLOR(background_red, 41);
MAKE_COLOR(background_green, 42);
MAKE_COLOR(background_yellow, 43);
MAKE_COLOR(background_blue, 44);
MAKE_COLOR(background_magenta, 45);
MAKE_COLOR(background_cyan, 46);
MAKE_COLOR(background_light_gray, 47);
MAKE_COLOR(background_default_, 49);
MAKE_COLOR(background_dark_gray, 100);
MAKE_COLOR(background_light_red, 101);
MAKE_COLOR(background_light_green, 102);
MAKE_COLOR(background_light_yellow, 103);
MAKE_COLOR(background_light_blue, 104);
MAKE_COLOR(background_light_magenta, 105);
MAKE_COLOR(background_light_cyan, 106);
MAKE_COLOR(background_white, 107);
}} // namespace show::colors
#undef MAKE_COLOR

#endif // SHOW_INCLUDE_SHOW_COLORS_HH_1586076787368070108_
