#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_CONCEPT_DEF_HH_1551621227203853866_
#define SHOW_INCLUDE_SHOW_CONCEPTS_CONCEPT_DEF_HH_1551621227203853866_

#include "../detail/config.hh"

#if SHOW_HAVE_CONCEPTS
#  define SHOW_CONCEPT concept
#elif SHOW_HAVE_CONCEPTS_TS
#  define SHOW_CONCEPT concept bool
#else
#  define SHOW_CONCEPT inline constexpr bool
#endif

#if SHOW_HAVE_CONCEPTS || SHOW_HAVE_CONCEPTS_TS
#  define SHOW_CONCEPT_RETURNS(...)                                            \
    {                                                                          \
      __VA_ARGS__;                                                             \
    }
#  define SHOW_CONCEPT_REQUIRES(name, ...)                                     \
    SHOW_CONCEPT name = requires(__VA_ARGS__) SHOW_CONCEPT_RETURNS
#  define SHOW_DEFINE_CONCEPT(name)

#else
#  define SHOW_CONCEPT_RETURNS(...) ->decltype(__VA_ARGS__)
#  define SHOW_CONCEPT_REQUIRES(name, ...)                                     \
    auto name##_requires(__VA_ARGS__) SHOW_CONCEPT_RETURNS

#  define SHOW_DEFINE_CONCEPT(name)                                            \
    template <typename T>                                                      \
    std::false_type name##_helper(...);                                        \
    template <typename T>                                                      \
    auto name##_helper(int)->decltype(&name##_requires<T>, std::true_type{});  \
    template <typename T>                                                      \
    SHOW_CONCEPT name = decltype(name##_helper<T>(0))::value;
#endif

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_CONCEPT_DEF_HH_1551621227203853866_
