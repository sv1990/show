#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_DEREFERENCABLE_HH_1539700442454157449_
#define SHOW_INCLUDE_SHOW_CONCEPTS_DEREFERENCABLE_HH_1539700442454157449_

#include "concept_def.hh"

#include <type_traits>

namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT_REQUIRES(Dereferencable, const T& x)
(*x);

SHOW_DEFINE_CONCEPT(Dereferencable)

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_DEREFERENCABLE_HH_1539700442454157449_
