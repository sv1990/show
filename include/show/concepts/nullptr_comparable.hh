#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_NULLPTR_COMPARABLE_HH_1539700905446953455_
#define SHOW_INCLUDE_SHOW_CONCEPTS_NULLPTR_COMPARABLE_HH_1539700905446953455_

#include "concept_def.hh"

#include <type_traits>

namespace show { namespace concepts {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnonnull"

template <typename T>
SHOW_CONCEPT_REQUIRES(NullptrComparable, const T& x)
(void(x == nullptr), void(x != nullptr));

#pragma GCC diagnostic pop

SHOW_DEFINE_CONCEPT(NullptrComparable)

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_NULLPTR_COMPARABLE_HH_1539700905446953455_
