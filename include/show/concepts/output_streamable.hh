#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_OUTPUT_STREAMABLE_HH_1539700751222195591_
#define SHOW_INCLUDE_SHOW_CONCEPTS_OUTPUT_STREAMABLE_HH_1539700751222195591_

#include "concept_def.hh"

#include <ostream>
#include <type_traits>

namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT_REQUIRES(OutputStreamable, const T& x, std::ostream& os)
(os << x);

SHOW_DEFINE_CONCEPT(OutputStreamable)

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_OUTPUT_STREAMABLE_HH_1539700751222195591_
