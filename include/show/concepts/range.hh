#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_RANGE_HH_1539700726326209150_
#define SHOW_INCLUDE_SHOW_CONCEPTS_RANGE_HH_1539700726326209150_

#include "concept_def.hh"

#include <iterator>
#include <type_traits>

namespace show { namespace concepts {
using std::begin;
using std::end;

template <typename T>
SHOW_CONCEPT_REQUIRES(Range, const T& x)
(begin(x), *begin(x), begin(x) == end(x), begin(x) != end(x));
// ++begin(x) didn't work :(

SHOW_DEFINE_CONCEPT(Range)

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_RANGE_HH_1539700726326209150_
