#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_STRUCT_HH_1539700730310127428_
#define SHOW_INCLUDE_SHOW_CONCEPTS_STRUCT_HH_1539700730310127428_

#include "range.hh"

#include "../detail/config.hh"

namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT Struct =
    std::is_class<T>::value&& std::is_aggregate<T>::value && !Range<T>;

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_STRUCT_HH_1539700730310127428_
