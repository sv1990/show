#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_TEMPLATE_DEF_HH_1539700384886165215_
#define SHOW_INCLUDE_SHOW_CONCEPTS_TEMPLATE_DEF_HH_1539700384886165215_

#include "../detail/config.hh"

#if SHOW_HAVE_CONCEPTS || SHOW_HAVE_CONCEPTS_TS
#  define SHOW_TEMPLATE(...)  template <__VA_ARGS__>
#  define SHOW_REQUIRES(cond) requires(cond)
#else
#  define SHOW_TEMPLATE(...)  template <__VA_ARGS__
#  define SHOW_REQUIRES(cond) , std::enable_if_t<cond>* = nullptr >
#endif

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_TEMPLATE_DEF_HH_1539700384886165215_
