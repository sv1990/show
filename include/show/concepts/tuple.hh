#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_TUPLE_HH_1539700814182879530_
#define SHOW_INCLUDE_SHOW_CONCEPTS_TUPLE_HH_1539700814182879530_

#include "concept_def.hh"

#include <tuple>

namespace show { namespace concepts {

namespace detail {
template <typename... Ts>
void tuple_tester(std::tuple<Ts...>);
template <typename T1, typename T2>
void tuple_tester(std::pair<T1, T2>);
} // namespace detail

template <typename T>
SHOW_CONCEPT_REQUIRES(Tuple, const T& x)
(detail::tuple_tester(x));

SHOW_DEFINE_CONCEPT(Tuple)
}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_TUPLE_HH_1539700814182879530_
