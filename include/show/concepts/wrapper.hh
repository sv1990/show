#ifndef SHOW_INCLUDE_SHOW_CONCEPTS_WRAPPER_HH_1576575691721966414_
#define SHOW_INCLUDE_SHOW_CONCEPTS_WRAPPER_HH_1576575691721966414_

#include "concept_def.hh"

namespace show { namespace concepts {
template <typename T>
SHOW_CONCEPT_REQUIRES(Wrapper, const T& x)
(x.value());

SHOW_DEFINE_CONCEPT(Wrapper)

template <typename T>
SHOW_CONCEPT_REQUIRES(Optional, const T& x)
(x.has_value(), x.value());

SHOW_DEFINE_CONCEPT(Optional)

}} // namespace show::concepts

#endif // SHOW_INCLUDE_SHOW_CONCEPTS_WRAPPER_HH_1576575691721966414_
