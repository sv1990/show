#ifndef SHOW_INCLUDE_SHOW_DBG_HH_1535386048321292189_
#define SHOW_INCLUDE_SHOW_DBG_HH_1535386048321292189_

#include "colors.hh"
#include "show.hh"

#include <iostream>

namespace show { namespace detail {
template <typename T>
decltype(auto) dbg(T&& x, const char* rep, const char* filename, int line) {
#ifndef SHOW_DISABLE_DBG
  std::clog << colors::light_gray << filename << '(' << line << "): " << rep
            << " = " << show(x) << colors::reset_all << '\n';
#endif
  return std::forward<T>(x);
}
}} // namespace show::detail

#define SHOW_DBG(x) show::detail::dbg(x, #x, __FILE__, __LINE__)

#ifndef SHOW_DISABLE_SHORT_DBG
#  define DBG SHOW_DBG
#endif

#endif // SHOW_INCLUDE_SHOW_DBG_HH_1535386048321292189_
