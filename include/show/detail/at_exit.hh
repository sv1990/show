#ifndef SHOW_INCLUDE_SHOW_DETAIL_AT_EXIT_HH_1535628740214252904_
#define SHOW_INCLUDE_SHOW_DETAIL_AT_EXIT_HH_1535628740214252904_

#include <utility>

namespace show { namespace detail {
template <typename Func>
class exit_runner {
  Func _func;

public:
  exit_runner(Func func) noexcept : _func(std::move(func)) {}
  ~exit_runner() { _func(); }
};

template <typename Func>
exit_runner<Func> at_exit(Func&& func) noexcept {
  return {std::forward<Func>(func)};
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_AT_EXIT_HH_1535628740214252904_
