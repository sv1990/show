#ifndef SHOW_INCLUDE_SHOW_DETAIL_CONFIG_HH_1530708704995956519_
#define SHOW_INCLUDE_SHOW_DETAIL_CONFIG_HH_1530708704995956519_

#include <type_traits>

#if __cplusplus < 201703L
#  error "show requires C++17"
#endif

#define SHOW_HAVE_CONCEPTS_TS (__cpp_concepts == 201507)
#define SHOW_HAVE_CONCEPTS    (__cpp_concepts > 201507)

#define SHOW_RETURNS(expr)                                                     \
  noexcept(noexcept(expr))->decltype(expr) { return expr; }

namespace show { namespace detail {
template <typename T>
struct delay_false : std::false_type {};
}} // namespace show::detail

// clang doesn't support "\n" in static_assert messages
#if defined(__GNUC__) && !defined(__clang__)
#  define SHOW_ERR_MSG(msg) "\n\n" msg "\n"
#else
#  define SHOW_ERR_MSG(msg) msg
#endif

#define SHOW_COMPILE_FAILURE(type, msg)                                        \
  static_assert(show::detail::delay_false<type>::value, SHOW_ERR_MSG(msg))

#endif // SHOW_INCLUDE_SHOW_DETAIL_CONFIG_HH_1530708704995956519_
