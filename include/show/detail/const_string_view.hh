#ifndef SHOW_INCLUDE_SHOW_DETAIL_CONST_STRING_VIEW_HH_1563617024338545169_
#define SHOW_INCLUDE_SHOW_DETAIL_CONST_STRING_VIEW_HH_1563617024338545169_

#include <algorithm>
#include <ostream>

namespace show { namespace detail {
class const_string_view {
  const char* _data = nullptr;
  std::size_t _size = 0;

public:
  explicit constexpr const_string_view(const char* data,
                                       std::size_t size) noexcept
      : _data(data), _size(size) {}
  template <std::size_t Size>
  explicit constexpr const_string_view(const char (&data)[Size]) noexcept
      : const_string_view(&data[0], Size - 1) {}
  constexpr const_string_view remove_prefix(std::size_t n) const noexcept {
    return const_string_view(_data + (n <= _size ? n : _size),
                             n <= _size ? _size - n : 0);
  }
  constexpr const_string_view remove_suffix(std::size_t n) const noexcept {
    return const_string_view(_data, n <= _size ? _size - n : 0);
  }
  constexpr const char* data() const noexcept { return _data; }
  constexpr std::size_t size() const noexcept { return _size; }
  constexpr char operator[](std::size_t i) const noexcept { return _data[i]; }
  constexpr auto begin() const noexcept { return &_data[0]; }
  constexpr auto end() const noexcept { return &_data[_size]; }
};

std::ostream& operator<<(std::ostream& os, const const_string_view& cs) {
  std::copy_n(cs.data(), cs.size(), std::ostreambuf_iterator<char>{os});
  return os;
}

}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_CONST_STRING_VIEW_HH_1563617024338545169_
