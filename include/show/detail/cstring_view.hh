#ifndef SHOW_INCLUDE_SHOW_DETAIL_CSTRING_VIEW_HH_1535443580250324294_
#define SHOW_INCLUDE_SHOW_DETAIL_CSTRING_VIEW_HH_1535443580250324294_

namespace show { namespace detail {

class cstring_view {
  const char* _str;

public:
  class sentinel {};
  class iterator {
    const char* _c;

  public:
    iterator(const char* c) : _c(c){};
    const char& operator*() const { return *_c; }
    iterator& operator++() {
      ++_c;
      return *this;
    }
    iterator operator++(int) {
      auto tmp = *this;
      ++(*this);
      return tmp;
    }
    bool operator==(sentinel) const { return *_c == '\0'; }
    bool operator!=(sentinel s) const { return !(*this == s); }
  };
  cstring_view(const char* str) noexcept : _str(str){};
  iterator begin() const noexcept { return {_str}; }
  sentinel end() const noexcept { return {}; }
};

}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_CSTRING_VIEW_HH_1535443580250324294_
