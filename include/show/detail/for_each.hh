#ifndef SHOW_INCLUDE_SHOW_DETAIL_FOR_EACH_HH_1535294107616821641_
#define SHOW_INCLUDE_SHOW_DETAIL_FOR_EACH_HH_1535294107616821641_

#include "../concepts/all.hh"

#include <iterator>

namespace show { namespace detail {
SHOW_TEMPLATE(typename Iter, typename Sent, typename Func)
SHOW_REQUIRES(concepts::Dereferencable<Iter>)
Iter for_each_n(Iter beg, Sent sent, std::ptrdiff_t n, Func&& func) {
  for (; beg != sent && n > 0; ++beg, --n) {
    func(*beg);
  }
  return beg;
}

SHOW_TEMPLATE(typename Rng, typename Func)
SHOW_REQUIRES(concepts::Range<Rng>)
auto for_each_n(Rng&& rng, std::ptrdiff_t n, Func&& func) {
  using std::begin;
  using std::end;
  return for_each_n(begin(rng), end(rng), n, std::forward<Func>(func));
}

SHOW_TEMPLATE(typename Iter, typename Sent, typename Func)
SHOW_REQUIRES(concepts::Dereferencable<Iter>)
Iter for_each(Iter beg, Sent sent, Func&& func) {
  for (; beg != sent; ++beg) {
    func(*beg);
  }
  return beg;
}

SHOW_TEMPLATE(typename Rng, typename Func)
SHOW_REQUIRES(concepts::Range<Rng>)
auto for_each(Rng&& rng, Func&& func) {
  using std::begin;
  using std::end;
  return detail::for_each(begin(rng), end(rng), std::forward<Func>(func));
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_FOR_EACH_HH_1535294107616821641_
