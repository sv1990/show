#ifndef SHOW_INCLUDE_SHOW_DETAIL_HAS_VALUE_IMPL_HH_1576577336936654782_
#define SHOW_INCLUDE_SHOW_DETAIL_HAS_VALUE_IMPL_HH_1576577336936654782_

#include "../concepts/template_def.hh"
#include "../concepts/wrapper.hh"

namespace show { namespace detail {
SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(concepts::Optional<T>)
bool has_value(const T& x) noexcept {
  return x.has_value();
}

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(!concepts::Optional<T>)
constexpr bool has_value(const T&) noexcept {
  return true;
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_HAS_VALUE_IMPL_HH_1576577336936654782_
