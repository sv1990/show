#ifndef SHOW_INCLUDE_SHOW_DETAIL_IS_NULLPTR_IMPL_HH_1576584054443284020_
#define SHOW_INCLUDE_SHOW_DETAIL_IS_NULLPTR_IMPL_HH_1576584054443284020_

#include "../concepts/nullptr_comparable.hh"
#include "../concepts/template_def.hh"

namespace show { namespace detail {
SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(concepts::NullptrComparable<T>)
bool is_nullptr(const T& x) noexcept {
  return x == nullptr;
}

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(!concepts::NullptrComparable<T>)
constexpr bool is_nullptr(const T&) noexcept {
  return false;
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_IS_NULLPTR_IMPL_HH_1576584054443284020_
