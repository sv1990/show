#ifndef SHOW_INCLUDE_SHOW_PRINTER_HH_1535453473058070815_
#define SHOW_INCLUDE_SHOW_PRINTER_HH_1535453473058070815_

#include "at_exit.hh"
#include "config.hh"
#include "cstring_view.hh"
#include "for_each.hh"
#include "has_value_impl.hh"
#include "is_nullptr_impl.hh"
#include "range_wrapper.hh"
#include "show_typename.hh"
#include "to_tuple.hh"
#include "tuple_for_each.hh"
#include "underlying_container.hh"

#include "../concepts/all.hh"

#include <algorithm>
#include <ostream>
#include <queue>
#include <sstream>
#include <stack>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <variant>

#if !defined(SHOW_NO_BOOST_VARIANT) || !defined(SHOW_NO_BOOST_TYPES)
#  if __has_include(<boost/variant.hpp>)
#    include <boost/variant.hpp>
#    define SHOW_BOOST_VARIANT 1
#  endif
#endif

namespace show { namespace detail {

/**
 * Prints every object to to a given ostream reference.
 *
 * Printer is a class instead of a set of function overloads to make it not
 * necessary to forward declare all function overloads to make them visible to
 * all other function overloads.
 */
class printer {
  std::ostream& os;

public:
  explicit printer(std::ostream& os_) noexcept : os(os_) {}

#ifndef SHOW_NOT_PRINTABLE_MESSAGE
#  define SHOW_NOT_PRINTABLE_MESSAGE "{show: not printable}"
#endif

#ifndef SHOW_DISABLE_UNPRINTABLE
  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(!concepts::OutputStreamable<T>
                && !concepts::Dereferencable<T>
                && !concepts::Range<T>
                && !concepts::Struct<T>
                && !concepts::Tuple<T>
                && !concepts::Wrapper<T>
                && !std::is_enum<T>::value)
  void print(const T&) noexcept {
#  ifndef SHOW_SILENT_UNPRINTABLE
    SHOW_COMPILE_FAILURE(
        T, "Type not printable. Please provide an overload for std::ostream& "
           "operator<<(std::ostream&, const T&) to fix this or define "
           "SHOW_SILENT_UNPRINTABLE before including show.hh to ignore this");
#  endif
    os << SHOW_NOT_PRINTABLE_MESSAGE;
  }
#endif

#if defined(SHOW_NUMERIC_STRINGS) && !defined(SHOW_NUMERIC_CHARS)
#  define SHOW_NUMERIC_CHARS
#endif

  void print(char c) noexcept {
#ifndef SHOW_NUMERIC_CHARS
    os << '\'' << c << '\'';
#else
    os << "(char)" << static_cast<int>(c);
#endif
  }

private:
  template <typename String>
  void print_string(const String& str) noexcept {
#ifndef SHOW_NUMERIC_STRINGS
    os << '\"' << str << '\"';
#else
    print(make_range(str));
#endif
  }

public:
  void print(const std::string& str) noexcept { print_string(str); }

  void print(std::string_view sv) noexcept { print_string(sv); }

  // For cstrings. Nothing has to be done
  void print(const char* c) noexcept {
    if (!c) {
      print(nullptr);
    }
    else {
#ifndef SHOW_NUMERIC_STRINGS
      os << '\"' << c << '\"';
#else
      print(cstring_view(c));
#endif
    }
  }

  void print(const std::ostringstream& oss) noexcept {
    os << "\"" << oss.rdbuf() << "\"";
  }

  template <typename T, std::size_t N>
  void print(const T (&arr)[N]) noexcept {
    print(make_range(arr));
  }

private:
  struct range_printer {
    printer* _this;
    bool first_elem = true;
    template <typename T>
    void operator()(T&& x) noexcept {
      if (!first_elem) {
        _this->os << ", ";
      }
      first_elem = false;
      _this->print(x);
    }
  };

public:
  SHOW_TEMPLATE(typename Tuple)
  SHOW_REQUIRES(concepts::Tuple<Tuple>)
  void print(const Tuple& tpl) noexcept {
    os << "(";
    tuple_for_each(tpl, range_printer{this});
    os << ")";
  }

  void print(bool b) noexcept {
#ifndef SHOW_NO_BOOL_ALPHA
    auto _ = at_exit([this, flags = os.flags()] { os.setf(flags); });
    os << std::boolalpha;
#endif
    os << b;
  }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::OutputStreamable<T>
                && !concepts::Dereferencable<T>
                && !std::is_enum<T>::value)
  void print(const T& x) noexcept { os << x; }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::Dereferencable<T> && !concepts::Wrapper<T>)
  void print(const T& x) noexcept {
    if (is_nullptr(x)) {
      print(nullptr);
    }
    else {
      print(*x);
    }
  }

  void print(std::nullptr_t) noexcept { os << "nullptr"; }

#ifndef SHOW_RANGE_LENGTH_LIMIT
#  define SHOW_RANGE_LENGTH_LIMIT 256
#endif

  SHOW_TEMPLATE(typename Rng)
  SHOW_REQUIRES(concepts::Range<Rng> && !concepts::OutputStreamable<Rng>)
  void print(const Rng& rng) noexcept {
    constexpr std::ptrdiff_t limit = SHOW_RANGE_LENGTH_LIMIT;

    os << '[';

    if constexpr (limit > 0) {
      auto last = for_each_n(rng, limit, range_printer{this});
      using std::end;
      if (last != end(rng)) {
        os << ", ...";
      }
    }
    else {
      for_each(rng, range_printer{this});
    }
    os << ']';
  }

  template <typename T, typename C>
  void print(const std::queue<T, C>& q) noexcept {
    print(detail::underlying_container(q));
  }

  template <typename T, typename C>
  void print(const std::stack<T, C>& s) noexcept {
    print(detail::underlying_container(s));
  }

  template <typename T, typename C, typename Comp>
  void print(const std::priority_queue<T, C, Comp>& pq) noexcept {
    print(detail::underlying_container(pq));
  }

  SHOW_TEMPLATE(typename E)
  SHOW_REQUIRES(std::is_enum<E>::value)
  void print(const E& e) noexcept {
    os << show_typename(e) << '('
       << static_cast<typename std::underlying_type<E>::type>(e) << ')';
  }

  SHOW_TEMPLATE(typename S)
  SHOW_REQUIRES(concepts::Struct<S>
                && !concepts::OutputStreamable<S>
                && !concepts::Wrapper<S>)
  void print(const S& s) {
    os << show_typename(s) << '{';
    tuple_for_each(to_tuple(s), range_printer{this});
    os << '}';
  }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::Wrapper<T>)
  void print(const T& x) {
    if (!has_value(x)) {
      os << "nullopt";
      return;
    }
    print(x.value());
  }

#if SHOW_BOOST_VARIANT
private:
  struct print_visitor : public boost::static_visitor<> {
    printer& p;
    print_visitor(printer& p_) noexcept : p(p_) {}
    template <typename T>
    void operator()(T&& x) noexcept {
      p.print(x);
    }
  };

public:
  template <typename... Ts>
  void print(const boost::variant<Ts...>& v) noexcept {
    print_visitor sv{*this};
    boost::apply_visitor(sv, v);
  }
#endif

  template <typename... Ts>
  void print(const std::variant<Ts...>& v) noexcept {
    std::visit([this](auto&& x) noexcept { return this->print(x); }, v);
  }
};
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_PRINTER_HH_1535453473058070815_
