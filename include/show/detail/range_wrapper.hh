#ifndef _SHOW_RANGE_WRAPPER_HH_1476868775440086884_
#define _SHOW_RANGE_WRAPPER_HH_1476868775440086884_

#include "../concepts/all.hh"

#include <iterator>

namespace show { namespace detail {
template <typename Iter, typename Sent = Iter>
class range_wrapper {
  Iter _beg;
  Sent _end;

public:
  using iterator = Iter;
  range_wrapper(Iter b, Sent e) : _beg(b), _end(e) {}
  auto begin() const { return _beg; }
  auto end() const { return _end; }
  auto cbegin() const { return _beg; }
  auto cend() const { return _end; }
};

template <typename Iter, typename Sent = Iter>
auto make_range(Iter b, Sent e) noexcept {
  return range_wrapper<Iter, Sent>{b, e};
}

template <typename Iter, typename Sent = Iter>
auto make_range(const std::pair<Iter, Sent>& p) noexcept {
  return make_range(p.first, p.second);
}

SHOW_TEMPLATE(typename Rng)
SHOW_REQUIRES(concepts::Range<Rng>)
auto make_range(const Rng& rng) noexcept {
  using std::begin;
  using std::end;
  return make_range(begin(rng), end(rng));
}

}} // namespace show::detail

#endif // _SHOW_RANGE_WRAPPER_HH_1476868775440086884_
