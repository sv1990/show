#ifndef SHOW_INCLUDE_SHOW_DETAIL_SEARCH_HH_1587535837641347565_
#define SHOW_INCLUDE_SHOW_DETAIL_SEARCH_HH_1587535837641347565_

#include <iterator>

template <class It1, class It2>
constexpr It1 search(It1 first, It1 last, It2 s_first, It2 s_last) {
  for (;; ++first) {
    It1 it = first;
    for (It2 s_it = s_first;; ++it, ++s_it) {
      if (s_it == s_last) {
        return first;
      }
      if (it == last) {
        return last;
      }
      if (!(*it == *s_it)) {
        break;
      }
    }
  }
}

template <typename Rng1, typename Rng2>
constexpr auto search(Rng1&& r1, Rng2&& r2) {
  using std::begin, std::end;
  return search(begin(r1), end(r1), begin(r2), end(r2));
}

#endif // SHOW_INCLUDE_SHOW_DETAIL_SEARCH_HH_1587535837641347565_
