#ifndef SHOW_INCLUDE_SHOW_DETAIL_SHOW_PROXY_HH_1556562023306127996_
#define SHOW_INCLUDE_SHOW_DETAIL_SHOW_PROXY_HH_1556562023306127996_

#include "printer.hh"

namespace show { namespace detail {
template <typename T>
struct is_tuple_of_crefs : std::false_type {};
template <typename... Ts>
struct is_tuple_of_crefs<std::tuple<const Ts&...>> : std::true_type {};

/**
 * Proxy class to delay showing the requested value as a string.
 *
 * It prevents creating a temporary string using a stringstream when that string
 * has to printed to a stream anyway. In that case the value is directly shown
 * in the stream without creating a temporary.
 */
template <typename T>
class show_proxy {
  // Otherwise std::tuple<const Ts&...>& would lead to a dangling reference in
  // the variadic case.
  using U = std::decay_t<T>;
  using stored_type =
      std::conditional_t<is_tuple_of_crefs<U>::value, U, const T&>;
  stored_type _val;

public:
  explicit show_proxy(const stored_type& x) noexcept : _val(x) {}
  operator std::string() && noexcept {
    std::ostringstream oss;
    printer(oss).print(_val);
    return std::move(oss).str();
  }
  operator std::string() const& noexcept {
    SHOW_COMPILE_FAILURE(T,
                         "The result of show should not be captured with auto");
    return "";
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  show_proxy<T>&& proxy) noexcept {
    printer(os).print(proxy._val);
    return os;
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  const show_proxy<T>&) noexcept {
    SHOW_COMPILE_FAILURE(T,
                         "The result of show should not be captured with auto");
    return os;
  }
};
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_SHOW_PROXY_HH_1556562023306127996_
