#ifndef SHOW_INCLUDE_SHOW_DETAIL_SHOW_TYPENAME_HH_1563617119320322925_
#define SHOW_INCLUDE_SHOW_DETAIL_SHOW_TYPENAME_HH_1563617119320322925_

#include "const_string_view.hh"
#include "search.hh"

namespace show { namespace detail {
template <typename T>
constexpr const_string_view tester() {
  return const_string_view(__PRETTY_FUNCTION__);
}

template <typename T = double>
constexpr std::pair<std::size_t, std::size_t> typename_indices() {
  auto s = tester<double>();
  const_string_view x("double");
  const auto it   = search(s, x);
  const auto pre  = it - s.begin();
  const auto suff = s.size() - pre - 6;
  return {pre, suff};
}

template <typename T>
constexpr const_string_view show_typename() {
  const auto [pre, suff] = typename_indices();
  const auto signature   = tester<T>();
  return signature.remove_prefix(pre).remove_suffix(suff);
}

template <typename T>
constexpr const_string_view show_typename(T&&) {
  return show_typename<typename std::decay<T>::type>();
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_SHOW_TYPENAME_HH_1563617119320322925_
