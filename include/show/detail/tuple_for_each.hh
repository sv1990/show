#ifndef SHOW_INCLUDE_SHOW_DETAIL_TUPLE_FOR_EACH_HH_1540645467601188727_
#define SHOW_INCLUDE_SHOW_DETAIL_TUPLE_FOR_EACH_HH_1540645467601188727_

#include <tuple>
#include <utility>

namespace show { namespace detail {
template <typename T>
struct tuple_size {
private:
  template <typename... Ts>
  static constexpr auto count(std::tuple<Ts...>)
      -> std::integral_constant<std::size_t, sizeof...(Ts)>;
  template <typename T1, typename T2>
  static constexpr auto count(std::pair<T1, T2>)
      -> std::integral_constant<std::size_t, 2>;

public:
  static constexpr std::size_t value =
      decltype(count(std::declval<const T&>()))::value;
};

template <typename Tuple, typename Func, std::size_t... Is>
void tuple_for_each(const Tuple& tpl, Func&& func,
                    std::index_sequence<Is...>) noexcept {
  (std::forward<Func>(func)(std::get<Is>(tpl)), ...);
}
template <typename Tuple, typename Func, std::size_t... Is>
void tuple_for_each(const Tuple&, Func&&, std::index_sequence<>) noexcept {
}
template <typename Tuple, typename Func>
void tuple_for_each(const Tuple& tpl, Func&& func) noexcept {
  tuple_for_each(tpl, std::forward<Func>(func),
                 std::make_index_sequence<
                     tuple_size<std::remove_reference_t<Tuple>>::value>{});
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_TUPLE_FOR_EACH_HH_1540645467601188727_
