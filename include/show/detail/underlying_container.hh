#ifndef SHOW_INCLUDE_SHOW_DETAIL_UNDERLYING_CONTAINER_HH_1537899880827315331_
#define SHOW_INCLUDE_SHOW_DETAIL_UNDERLYING_CONTAINER_HH_1537899880827315331_

#include <queue>
#include <stack>

namespace show { namespace detail {
template <typename T, typename C>
struct queue_inspector : std::queue<T, C> {
  explicit queue_inspector(const std::queue<T, C>& q) noexcept
      : std::queue<T, C>(q) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C>
const C& underlying_container(const std::queue<T, C>& q) noexcept {
  return reinterpret_cast<const detail::queue_inspector<T, C>*>(&q)
      ->container();
}

template <typename T, typename C>
struct stack_inspector : std::stack<T, C> {
  explicit stack_inspector(const std::stack<T, C>& s) noexcept
      : std::stack<T, C>(s) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C>
const C& underlying_container(const std::stack<T, C>& s) noexcept {
  return reinterpret_cast<const detail::stack_inspector<T, C>*>(&s)
      ->container();
}

template <typename T, typename C, typename Comp>
struct priority_queue_inspector : std::priority_queue<T, C, Comp> {
  explicit priority_queue_inspector(
      const std::priority_queue<T, C, Comp>& pq) noexcept
      : std::priority_queue<T, C, Comp>(pq) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C, typename Comp>
const C&
underlying_container(const std::priority_queue<T, C, Comp>& pq) noexcept {
  return reinterpret_cast<const detail::priority_queue_inspector<T, C, Comp>*>(
             &pq)
      ->container();
}
}} // namespace show::detail

#endif // SHOW_INCLUDE_SHOW_DETAIL_UNDERLYING_CONTAINER_HH_1537899880827315331_
