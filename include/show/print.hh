#ifndef SHOW_INCLUDE_SHOW_PRINT_HH_1556561929166593756_
#define SHOW_INCLUDE_SHOW_PRINT_HH_1556561929166593756_

#include "show.hh"

#include <iostream>

namespace show {
SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void print(std::ostream& os, const Ts&... xs) noexcept {
  os << show(xs...);
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void print(const Ts&... xs) noexcept {
  print(std::cout, xs...);
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void eprint(const Ts&... xs) noexcept {
  print(std::clog, xs...);
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void println(std::ostream& os, const Ts&... xs) noexcept {
  os << show(xs...) << '\n';
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void println(const Ts&... xs) noexcept {
  println(std::cout, xs...);
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 1)
void eprintln(const Ts&... xs) noexcept {
  println(std::clog, xs...);
}
} // namespace show

#endif // SHOW_INCLUDE_SHOW_PRINT_HH_1556561929166593756_
