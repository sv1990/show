#ifndef _SHOW_SHOW_HH_1476868914927124867_
#define _SHOW_SHOW_HH_1476868914927124867_

#include "detail/show_proxy.hh"

namespace show {

template <typename T>
detail::show_proxy<T> show(const T& x) noexcept {
  return detail::show_proxy<T>{x};
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 2)
detail::show_proxy<std::tuple<const Ts&...>> show(const Ts&... xs) noexcept {
  return show(std::tie(xs...));
}

// Eager version of show
template <typename T>
std::string to_string(const T& x) noexcept {
  return static_cast<std::string>(show(x));
}

} // namespace show
#endif // _SHOW_SHOW_HH_1476868914927124867_
