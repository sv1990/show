#ifndef SHOW_INCLUDE_SHOW_SHOW_PREPROCESSED_HH_1567506606908996644_
#define SHOW_INCLUDE_SHOW_SHOW_PREPROCESSED_HH_1567506606908996644_

#define SHOW_STRINGIFY_(x) #x
#define SHOW_STRINGIFY(x)  SHOW_STRINGIFY_(x)

#define SHOW_PRINT_PREPROCESSED(x)                                             \
  std::cout << #x << " = " << SHOW_STRINGIFY(x) << '\n';

#endif // SHOW_INCLUDE_SHOW_SHOW_PREPROCESSED_HH_1567506606908996644_
