#ifndef SHOW_INCLUDE_SHOW_SHOW_TYPE_HH_1530789118995194468_
#define SHOW_INCLUDE_SHOW_SHOW_TYPE_HH_1530789118995194468_

#include <type_traits>

namespace show {
// The deprecated trick was motivated by
// https://cukic.co/2019/02/19/tmp-testing-and-debugging-templates/

/**
 * Shows the types Ts... at compile type by provoking a deprecation warning
 */
template <typename... Ts>
struct [[deprecated]] show_type {};

/**
 * Shows the types Ts... at compile type either by provoking a deprecation
 * warning
 */
template <typename... Ts>
[[deprecated]] inline auto show_value_type(Ts&&...) {
}

/**
 * If argument is of std::false_type it will lead to a deprecation warning
 *
 */
inline auto static_log(std::true_type) {
}
[[deprecated]] inline auto static_log(std::false_type) {
}
} // namespace show

#endif // SHOW_INCLUDE_SHOW_SHOW_TYPE_HH_1530789118995194468_
