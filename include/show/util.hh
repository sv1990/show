#ifndef SHOW_INCLUDE_SHOW_UTIL_HH_1547492015563895030_
#define SHOW_INCLUDE_SHOW_UTIL_HH_1547492015563895030_

#include "detail/config.hh"
#include "detail/range_wrapper.hh"

namespace show {
using detail::make_range;
} // namespace show

#endif // SHOW_INCLUDE_SHOW_UTIL_HH_1547492015563895030_
