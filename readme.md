# Show

## Motivation

In most languages like Python, Haskell or Rust it is easy to print a collection to the terminal without having to write a loop.

Python:

```python
ls = [1, 2, 3]
print(ls)
# prints [1, 2, 3]
```

Rust:

```rust
let ls = vec![1, 2, 3];
println!("{:?}", ls);
// prints [1, 2, 3]
```

Haskell

```haskell
let ls = [1, 2, 3]
putStrLn (show ls)
print ls -- this is equivalent to the line above
-- prints [1, 2, 3]
```

This also scales well to nested lists/vectors or tuples.

User defined types have to have a `__str__` member function or to simply be declared as `#[derive(Debug)]` in Rust or `deriving Show` in Haskell.

C++ offers nothing similar. Instead one has to write loops or use stl algorithms to print a simple collection. For nested structures one has to use nested loops and for tuples one has to use template programming or hand-coding. This is especially annoying if one just want to print something for debugging. This library tries to offer the same comfort as above.

C++ (this library)

```c++
#include <show/all.hh>
std::vector ls{1, 2, 3};
std::cout << show::show(ls) << '\n';
show::print(ls); // -- this is equivalent to the line above
// prints [1, 2, 3]
```

## The Function

The output of `show`  mimics the output of the function `show` from Haskell:

* *chars* printed with `'` quotes
* *std::strings* and *raw strings* are printed with `"` quotes
* *Ranges*  (objects on which one can call `begin` and `end`) are denoted by `[` and `]`
* *Tuples*, *Pairs* and *Structs* (with up to 16 members, requires at least C++17) are denoted by `(` and `)`.
* The container adapters `std::stack`, `std::queue` and `std::priotity_queue` are printed by printing the underlying container. The order of the elements in the output not necessarily ordered by any means.

Other than that `show` can also print `(std|std::experimental|boost)::optional` and `(std|boost)::variant`.  The `experimental` and `boost` features can be disabled by defining either `SHOW_NO_(BOOST|EXPERIMENTAL)_TYPES` or `SHOW_NO_(BOOST|EXPERIMENTAL)_(VARIANT|OPTIONAL|STRING_VIEW)`.

Pointers and iterators are automatically dereferenced.

User defined types `T` can also be made showable by providing `std::ostream& operator<<(std::ostream&, const T&)`.

## `SHOW_DBG`

Additionally there is a helper macro `DBG` inspired by this [article](https://www.fluentcpp.com/2017/09/29/tee-getting-precious-debug-info-easily/) and rust's `dbg!` macro. If there is a need to print a intermediate value in a expression such as the return value of `g(x)` in `f(g(x))`. One can do that as `f(DBG(g(x)))`. This will print `g(x) = ...` to `STDOUT` without changing the value category of object returned by `g(x)`.

Alternatively one can write `f(g(x) | show::dbg)`. This is closer to the bash syntax but loses the ability to print the name of variable.

If the short name `DBG` leads conflicts, one can define `SHOW_DISABLE_SHORT_DBG` and use `SHOW_DBG` instead. If one defines `SHOW_DISABLE_DBG` before including `show.hh` nothing will be printed to the console.

## `SHOW_ASSERT`

This library consists also of simple assert macros leveraging the ability to show almost every type. The syntax is

```C++
#include <show/assert.hh>
std::vector<int> v1{1, 2, 3}, v2{1, 2, 4};
ASSERT_EQ(v1, v2); // The error message prints the contents of v1 and v2
```

In a future version it will probably be possible to write `ASSERT(v1 == v2)` and get the same result.

## `show_type` and `show_value_type`

When debugging generic contexts one is sometimes interested in getting the a concrete type for a template parameter `T` or a template variable `x`. This can be done by the class `show::show_type` and by the function `show::show_value_type` that produce compile warnings containing the concrete type:

```C++
#include <show/show_type.hh>
template <typename Iter>
void f(Iter beg, Iter end) {
	show::show_type<Iter> _;
}

std::vector<int> v{1, 2, 3};
f(begin(v), end(v));
```

Compiling with gcc 8.3.0 leads to the following warning

```bash
show_type.cc: In function ‘void f(Iter, Iter)’:
show_type.cc:6:8: warning: ‘template<class ... Ts> struct show::show_type’ is deprecated [-Wdeprecated-declarations]
  show::show_type<Iter> _;
        ^~~~~~~~~
In file included from show_type.cc:1:
/usr/local/include/show/show_type.hh:14:23: note: declared here
 struct [[deprecated]] show_type{};
                       ^~~~~~~~~
show_type.cc: In instantiation of ‘void f(Iter, Iter) [with Iter = __gnu_cxx::__normal_iterator<int*, std::vector<int> >]’:
```

From line 9 we can read the type `__gnu_cxx::__normal_iterator<int*, std::vector<int, std::allocator<int>>>`.

##  Installation

To install `show` clone the repository execute the following steps

```bash
git clone git@bitbucket.org:sv1990/show.git
cd show
mkdir build && cd build
cmake -DSHOW_ENABLE_TESTS=off -DSHOW_ENABLE_BENCHMARK=off .. && sudo make install
```

Since this is a header only library no compilation apart from the tests in necessary.

Additionally one can find single header versions of this library in the folder `single-header`. In the subfolder `core` is only the function `show::show` and in the folder `all` is a single header also containing `DBG` or `show_type`.

## Configuration

There are several macros that can be used to alter the behavior of `show`:

* `SHOW_SILENT_UNPRINTABLE`: When a type cannot be printed by `show` it leads to a compilation failure. If `SHOW_SILENT_UNPRINTABLE` was defined before including `show` a message defined by `SHOW_NOT_PRINTABLE_MESSAGE` (defaults to `"{show: not printable}"`) will be printed as a placeholder.
* `SHOW_DISABLE_UNPRINTABLE`: In some cases it can be useful to disable the overload for not printable types `T` since it is a better match for the overload resolution than casting `T` to a printable type even if that is possible.
* `SHOW_NO_BOOL_ALPHA`: If defined Boolean values will be printed as numbers instead of `true`/`false`.
* `SHOW_RANGE_LENGTH_LIMIT`:  Ranges are only printed up to a given threshold length (default: `256`). By defining this macro one can alter this value. If set to a negative value, it will probably print infinitely.
* `SHOW_NUMERIC_CHARS` & `SHOW_NUMERIC_STRINGS`: If defined, chars will be printed as `(char)num` instead of letters and strings will be printed like ranges of such numbers. `SHOW_NUMERIC_STRINGS` implies `SHOW_NUMERIC_CHARS`.
