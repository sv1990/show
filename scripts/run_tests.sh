#!/bin/bash

# Run tests for cmake and gcc with with resp. c++14 and c++17 enabled

run_tests() {
  compiler="$1"
  standard="$2"
  concept="$3"
  folder=".build_${compiler}${standard}_concepts_${concept}"
  mkdir -p "${folder}"
  cd "${folder}" || exit 1
  cmake -DCMAKE_CXX_COMPILER="${compiler}" -DCMAKE_CXX_STANDARD="${standard}" -DSHOW_USE_CONCEPTS="${concept}" ..
  make -j$(nproc) && ctest -j$(nproc)
  cd ..
}

compilers=("g++" "clang++")
standards=("14" "17")
concepts=("on" "off")

for compiler in "${compilers[@]}"; do
  for standard in "${standards[@]}"; do
    for concept in "${concepts[@]}"; do
      if ! run_tests "${compiler}" "${standard}" "${concept}" ; then
        echo "Failure for ${compiler} with c++${standard} and concepts=${concept}"
        exit 1
      fi
    done
  done
done

