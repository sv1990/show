#!/usr/bin/python

def args(n):
    return ", ".join(["_" + str(i) for i in range(1, n+1)])

def to_tuple_n(n):
    if n != 1:
        return "if constexpr (is_braces_constructible<type, {0}>{{}}) {{auto&& [{1}] = object; return std::tie({1});}}".format(", ".join(["any_type" for _ in range(n)]), args(n))
    else:
        return "if constexpr (is_braces_constructible<type, {0}>{{}}) {{auto&& [{1}] = object; return std::tie({1});}}".format("any_type_except<type>", "_1")

def to_tuple(n):
    return " else ".join([to_tuple_n(i) for i in reversed(range(1, n+1))]) + " else { return std::make_tuple(); }"

if __name__ == "__main__":
    n = 16
    print(to_tuple(n))
