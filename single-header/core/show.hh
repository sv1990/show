// MIT License
// 
// Copyright (c) 2019 Semir Vrana
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice (including the next
// paragraph) shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef SINGLE_HEADER_SHOW_HH
#define SINGLE_HEADER_SHOW_HH
#include <algorithm>
#include <iterator>
#include <ostream>
#include <queue>
#include <sstream>
#include <stack>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <variant>


namespace show { namespace detail {
template <typename Func>
class exit_runner {
  Func _func;

public:
  exit_runner(Func func) noexcept : _func(std::move(func)) {}
  ~exit_runner() { _func(); }
};

template <typename Func>
exit_runner<Func> at_exit(Func&& func) noexcept {
  return {std::forward<Func>(func)};
}
}} // namespace show::detail



#if __cplusplus < 201703L
#  error "show requires C++17"
#endif

#define SHOW_HAVE_CONCEPTS_TS (__cpp_concepts == 201507)
#define SHOW_HAVE_CONCEPTS    (__cpp_concepts > 201507)

#define SHOW_RETURNS(expr)                                                     \
  noexcept(noexcept(expr))->decltype(expr) { return expr; }

namespace show { namespace detail {
template <typename T>
struct delay_false : std::false_type {};
}} // namespace show::detail

// clang doesn't support "\n" in static_assert messages
#if defined(__GNUC__) && !defined(__clang__)
#  define SHOW_ERR_MSG(msg) "\n\n" msg "\n"
#else
#  define SHOW_ERR_MSG(msg) msg
#endif

#define SHOW_COMPILE_FAILURE(type, msg)                                        \
  static_assert(show::detail::delay_false<type>::value, SHOW_ERR_MSG(msg))


namespace show { namespace detail {

class cstring_view {
  const char* _str;

public:
  class sentinel {};
  class iterator {
    const char* _c;

  public:
    iterator(const char* c) : _c(c){};
    const char& operator*() const { return *_c; }
    iterator& operator++() {
      ++_c;
      return *this;
    }
    iterator operator++(int) {
      auto tmp = *this;
      ++(*this);
      return tmp;
    }
    bool operator==(sentinel) const { return *_c == '\0'; }
    bool operator!=(sentinel s) const { return !(*this == s); }
  };
  cstring_view(const char* str) noexcept : _str(str){};
  iterator begin() const noexcept { return {_str}; }
  sentinel end() const noexcept { return {}; }
};

}} // namespace show::detail



#if SHOW_HAVE_CONCEPTS
#  define SHOW_CONCEPT concept
#elif SHOW_HAVE_CONCEPTS_TS
#  define SHOW_CONCEPT concept bool
#else
#  define SHOW_CONCEPT inline constexpr bool
#endif

#if SHOW_HAVE_CONCEPTS || SHOW_HAVE_CONCEPTS_TS
#  define SHOW_CONCEPT_RETURNS(...)                                            \
    { __VA_ARGS__; }
#  define SHOW_CONCEPT_REQUIRES(name, ...)                                     \
    SHOW_CONCEPT name = requires(__VA_ARGS__) SHOW_CONCEPT_RETURNS
#  define SHOW_DEFINE_CONCEPT(name)

#else
#  define SHOW_CONCEPT_RETURNS(...) ->decltype(__VA_ARGS__)
#  define SHOW_CONCEPT_REQUIRES(name, ...)                                     \
    auto name##_requires(__VA_ARGS__) SHOW_CONCEPT_RETURNS

#  define SHOW_DEFINE_CONCEPT(name)                                            \
    template <typename T>                                                      \
    std::false_type name##_helper(...);                                        \
    template <typename T>                                                      \
    auto name##_helper(int)->decltype(&name##_requires<T>, std::true_type{});  \
    template <typename T>                                                      \
    SHOW_CONCEPT name = decltype(name##_helper<T>(0))::value;
#endif




namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT_REQUIRES(Dereferencable, const T& x)
(*x);

SHOW_DEFINE_CONCEPT(Dereferencable)

}} // namespace show::concepts




namespace show { namespace concepts {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnonnull"

template <typename T>
SHOW_CONCEPT_REQUIRES(NullptrComparable, const T& x)
(void(x == nullptr), void(x != nullptr));

#pragma GCC diagnostic pop

SHOW_DEFINE_CONCEPT(NullptrComparable)

}} // namespace show::concepts




namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT_REQUIRES(OutputStreamable, const T& x, std::ostream& os)
(os << x);

SHOW_DEFINE_CONCEPT(OutputStreamable)

}} // namespace show::concepts




namespace show { namespace concepts {
using std::begin;
using std::end;

template <typename T>
SHOW_CONCEPT_REQUIRES(Range, const T& x)
(begin(x), *begin(x), begin(x) == end(x), begin(x) != end(x));
// ++begin(x) didn't work :(

SHOW_DEFINE_CONCEPT(Range)

}} // namespace show::concepts




namespace show { namespace concepts {

template <typename T>
SHOW_CONCEPT Struct =
    std::is_class<T>::value&& std::is_aggregate<T>::value && !Range<T>;

}} // namespace show::concepts



#if SHOW_HAVE_CONCEPTS || SHOW_HAVE_CONCEPTS_TS
#  define SHOW_TEMPLATE(...)  template <__VA_ARGS__>
#  define SHOW_REQUIRES(cond) requires(cond)
#else
#  define SHOW_TEMPLATE(...)  template <__VA_ARGS__
#  define SHOW_REQUIRES(cond) , std::enable_if_t<cond>* = nullptr >
#endif




namespace show { namespace concepts {

namespace detail {
template <typename... Ts>
void tuple_tester(std::tuple<Ts...>);
template <typename T1, typename T2>
void tuple_tester(std::pair<T1, T2>);
} // namespace detail

template <typename T>
SHOW_CONCEPT_REQUIRES(Tuple, const T& x)
(detail::tuple_tester(x));

SHOW_DEFINE_CONCEPT(Tuple)
}} // namespace show::concepts



namespace show { namespace concepts {
template <typename T>
SHOW_CONCEPT_REQUIRES(Wrapper, const T& x)
(x.value());

SHOW_DEFINE_CONCEPT(Wrapper)

template <typename T>
SHOW_CONCEPT_REQUIRES(Optional, const T& x)
(x.has_value(), x.value());

SHOW_DEFINE_CONCEPT(Optional)

}} // namespace show::concepts






namespace show { namespace detail {
SHOW_TEMPLATE(typename Iter, typename Sent, typename Func)
SHOW_REQUIRES(concepts::Dereferencable<Iter>)
Iter for_each_n(Iter beg, Sent sent, std::ptrdiff_t n, Func&& func) {
  for (; beg != sent && n > 0; ++beg, --n) {
    func(*beg);
  }
  return beg;
}

SHOW_TEMPLATE(typename Rng, typename Func)
SHOW_REQUIRES(concepts::Range<Rng>)
auto for_each_n(Rng&& rng, std::ptrdiff_t n, Func&& func) {
  using std::begin;
  using std::end;
  return for_each_n(begin(rng), end(rng), n, std::forward<Func>(func));
}

SHOW_TEMPLATE(typename Iter, typename Sent, typename Func)
SHOW_REQUIRES(concepts::Dereferencable<Iter>)
Iter for_each(Iter beg, Sent sent, Func&& func) {
  for (; beg != sent; ++beg) {
    func(*beg);
  }
  return beg;
}

SHOW_TEMPLATE(typename Rng, typename Func)
SHOW_REQUIRES(concepts::Range<Rng>)
auto for_each(Rng&& rng, Func&& func) {
  using std::begin;
  using std::end;
  return detail::for_each(begin(rng), end(rng), std::forward<Func>(func));
}
}} // namespace show::detail



namespace show { namespace detail {
SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(concepts::Optional<T>)
bool has_value(const T& x) noexcept {
  return x.has_value();
}

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(!concepts::Optional<T>)
constexpr bool has_value(const T&) noexcept {
  return true;
}
}} // namespace show::detail



namespace show { namespace detail {
SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(concepts::NullptrComparable<T>)
bool is_nullptr(const T& x) noexcept {
  return x == nullptr;
}

SHOW_TEMPLATE(typename T)
SHOW_REQUIRES(!concepts::NullptrComparable<T>)
constexpr bool is_nullptr(const T&) noexcept {
  return false;
}
}} // namespace show::detail




namespace show { namespace detail {
template <typename Iter, typename Sent = Iter>
class range_wrapper {
  Iter _beg;
  Sent _end;

public:
  using iterator = Iter;
  range_wrapper(Iter b, Sent e) : _beg(b), _end(e) {}
  auto begin() const { return _beg; }
  auto end() const { return _end; }
  auto cbegin() const { return _beg; }
  auto cend() const { return _end; }
};

template <typename Iter, typename Sent = Iter>
auto make_range(Iter b, Sent e) noexcept {
  return range_wrapper<Iter, Sent>{b, e};
}

template <typename Iter, typename Sent = Iter>
auto make_range(const std::pair<Iter, Sent>& p) noexcept {
  return make_range(p.first, p.second);
}

SHOW_TEMPLATE(typename Rng)
SHOW_REQUIRES(concepts::Range<Rng>)
auto make_range(const Rng& rng) noexcept {
  using std::begin;
  using std::end;
  return make_range(begin(rng), end(rng));
}

}} // namespace show::detail



namespace show { namespace detail {
class const_string_view {
  const char* _data = nullptr;
  std::size_t _size = 0;

public:
  explicit constexpr const_string_view(const char* data,
                                       std::size_t size) noexcept
      : _data(data), _size(size) {}
  template <std::size_t Size>
  explicit constexpr const_string_view(const char (&data)[Size]) noexcept
      : const_string_view(&data[0], Size - 1) {}
  constexpr const_string_view remove_prefix(std::size_t n) const noexcept {
    return const_string_view(_data + (n <= _size ? n : _size),
                             n <= _size ? _size - n : 0);
  }
  constexpr const_string_view remove_suffix(std::size_t n) const noexcept {
    return const_string_view(_data, n <= _size ? _size - n : 0);
  }
  constexpr const char* data() const noexcept { return _data; }
  constexpr std::size_t size() const noexcept { return _size; }
  constexpr char operator[](std::size_t i) const noexcept { return _data[i]; }
  constexpr auto begin() const noexcept { return &_data[0]; }
  constexpr auto end() const noexcept { return &_data[_size]; }
};

std::ostream& operator<<(std::ostream& os, const const_string_view& cs) {
  std::copy_n(cs.data(), cs.size(), std::ostreambuf_iterator<char>{os});
  return os;
}

}} // namespace show::detail



template <class It1, class It2>
constexpr It1 search(It1 first, It1 last, It2 s_first, It2 s_last) {
  for (;; ++first) {
    It1 it = first;
    for (It2 s_it = s_first;; ++it, ++s_it) {
      if (s_it == s_last) {
        return first;
      }
      if (it == last) {
        return last;
      }
      if (!(*it == *s_it)) {
        break;
      }
    }
  }
}

template <typename Rng1, typename Rng2>
constexpr auto search(Rng1&& r1, Rng2&& r2) {
  using std::begin, std::end;
  return search(begin(r1), end(r1), begin(r2), end(r2));
}



namespace show { namespace detail {
template <typename T>
constexpr const_string_view tester() {
  return const_string_view(__PRETTY_FUNCTION__);
}

template <typename T = double>
constexpr std::pair<std::size_t, std::size_t> typename_indices() {
  auto s = tester<double>();
  const_string_view x("double");
  const auto it   = search(s, x);
  const auto pre  = it - s.begin();
  const auto suff = s.size() - pre - 6;
  return {pre, suff};
}

template <typename T>
constexpr const_string_view show_typename() {
  const auto [pre, suff] = typename_indices();
  const auto signature   = tester<T>();
  return signature.remove_prefix(pre).remove_suffix(suff);
}

template <typename T>
constexpr const_string_view show_typename(T&&) {
  return show_typename<typename std::decay<T>::type>();
}
}} // namespace show::detail




// https://www.reddit.com/r/cpp/comments/4yp7fv/c17_structured_bindings_convert_struct_to_a_tuple/
namespace show { namespace detail {
template <class T, class... TArgs>
decltype(void(T{std::declval<TArgs>()...}), std::true_type{})
test_is_braces_constructible(int);

template <class, class...>
std::false_type test_is_braces_constructible(...);

template <class T, class... TArgs>
using is_braces_constructible =
    decltype(test_is_braces_constructible<T, TArgs...>(0));

// Use this to disable the copy constructor in the one argument case
template <typename T>
struct any_type_except {
  SHOW_TEMPLATE(typename U)
  SHOW_REQUIRES((!std::is_same_v<T, U>))
  constexpr operator U(); // non explicit
};

struct any_type {
  template <class T>
  constexpr operator T(); // non explicit
};

template <class T>
auto to_tuple(T&& object) noexcept {
  using type = std::decay_t<T>;

  static_assert(std::is_class_v<type> && std::is_aggregate_v<type>,
                "show::to_tuple can only be used with plain structs");

  // Generated by scripts/to_tuple.py

  if constexpr (is_braces_constructible<
                    type, any_type, any_type, any_type, any_type, any_type,
                    any_type, any_type, any_type, any_type, any_type, any_type,
                    any_type, any_type, any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15,
           _16] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,
                    _15, _16);
  }
  else if constexpr (is_braces_constructible<
                         type, any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15] =
        object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,
                    _15);
  }
  else if constexpr (is_braces_constructible<
                         type, any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14] =
        object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13,
                    _14);
  }
  else if constexpr (is_braces_constructible<
                         type, any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11);
  }
  else if constexpr (is_braces_constructible<
                         type, any_type, any_type, any_type, any_type, any_type,
                         any_type, any_type, any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9, _10] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8, _9] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8, _9);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7, _8] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7, _8);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type,
                                             any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6, _7] = object;
    return std::tie(_1, _2, _3, _4, _5, _6, _7);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5, _6] = object;
    return std::tie(_1, _2, _3, _4, _5, _6);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type, any_type>{}) {
    auto& [_1, _2, _3, _4, _5] = object;
    return std::tie(_1, _2, _3, _4, _5);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type, any_type,
                                             any_type>{}) {
    auto& [_1, _2, _3, _4] = object;
    return std::tie(_1, _2, _3, _4);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type,
                                             any_type>{}) {
    auto& [_1, _2, _3] = object;
    return std::tie(_1, _2, _3);
  }
  else if constexpr (is_braces_constructible<type, any_type, any_type>{}) {
    auto& [_1, _2] = object;
    return std::tie(_1, _2);
  }
  else if constexpr (is_braces_constructible<type, any_type_except<type>>{}) {
    auto& [_1] = object;
    return std::tie(_1);
  }
  else {
    return std::make_tuple();
  }
}
}} // namespace show::detail



namespace show { namespace detail {
template <typename T>
struct tuple_size {
private:
  template <typename... Ts>
  static constexpr auto count(std::tuple<Ts...>)
      -> std::integral_constant<std::size_t, sizeof...(Ts)>;
  template <typename T1, typename T2>
  static constexpr auto count(std::pair<T1, T2>)
      -> std::integral_constant<std::size_t, 2>;

public:
  static constexpr std::size_t value =
      decltype(count(std::declval<const T&>()))::value;
};

template <typename Tuple, typename Func, std::size_t... Is>
void tuple_for_each(const Tuple& tpl, Func&& func,
                    std::index_sequence<Is...>) noexcept {
  (std::forward<Func>(func)(std::get<Is>(tpl)), ...);
}
template <typename Tuple, typename Func, std::size_t... Is>
void tuple_for_each(const Tuple&, Func&&, std::index_sequence<>) noexcept {
}
template <typename Tuple, typename Func>
void tuple_for_each(const Tuple& tpl, Func&& func) noexcept {
  tuple_for_each(tpl, std::forward<Func>(func),
                 std::make_index_sequence<
                     tuple_size<std::remove_reference_t<Tuple>>::value>{});
}
}} // namespace show::detail



namespace show { namespace detail {
template <typename T, typename C>
struct queue_inspector : std::queue<T, C> {
  explicit queue_inspector(const std::queue<T, C>& q) noexcept
      : std::queue<T, C>(q) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C>
const C& underlying_container(const std::queue<T, C>& q) noexcept {
  return reinterpret_cast<const detail::queue_inspector<T, C>*>(&q)
      ->container();
}

template <typename T, typename C>
struct stack_inspector : std::stack<T, C> {
  explicit stack_inspector(const std::stack<T, C>& s) noexcept
      : std::stack<T, C>(s) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C>
const C& underlying_container(const std::stack<T, C>& s) noexcept {
  return reinterpret_cast<const detail::stack_inspector<T, C>*>(&s)
      ->container();
}

template <typename T, typename C, typename Comp>
struct priority_queue_inspector : std::priority_queue<T, C, Comp> {
  explicit priority_queue_inspector(
      const std::priority_queue<T, C, Comp>& pq) noexcept
      : std::priority_queue<T, C, Comp>(pq) {}
  const C& container() const noexcept { return this->c; }
};

template <typename T, typename C, typename Comp>
const C&
underlying_container(const std::priority_queue<T, C, Comp>& pq) noexcept {
  return reinterpret_cast<const detail::priority_queue_inspector<T, C, Comp>*>(
             &pq)
      ->container();
}
}} // namespace show::detail





#if !defined(SHOW_NO_BOOST_VARIANT) || !defined(SHOW_NO_BOOST_TYPES)
#  if __has_include(<boost/variant.hpp>)
#    include <boost/variant.hpp>
#    define SHOW_BOOST_VARIANT 1
#  endif
#endif

namespace show { namespace detail {

/**
 * Prints every object to to a given ostream reference.
 *
 * Printer is a class instead of a set of function overloads to make it not
 * necessary to forward declare all function overloads to make them visible to
 * all other function overloads.
 */
class printer {
  std::ostream& os;

public:
  explicit printer(std::ostream& os_) noexcept : os(os_) {}

#ifndef SHOW_NOT_PRINTABLE_MESSAGE
#  define SHOW_NOT_PRINTABLE_MESSAGE "{show: not printable}"
#endif

#ifndef SHOW_DISABLE_UNPRINTABLE
  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(!concepts::OutputStreamable<T> &&
                !concepts::Dereferencable<T> && !concepts::Range<T> &&
                !concepts::Struct<T> && !concepts::Tuple<T> &&
                !concepts::Wrapper<T> && !std::is_enum<T>::value)
  void print(const T&) noexcept {
#  ifndef SHOW_SILENT_UNPRINTABLE
    SHOW_COMPILE_FAILURE(
        T, "Type not printable. Please provide an overload for std::ostream& "
           "operator<<(std::ostream&, const T&) to fix this or define "
           "SHOW_SILENT_UNPRINTABLE before including show.hh to ignore this");
#  endif
    os << SHOW_NOT_PRINTABLE_MESSAGE;
  }
#endif

#if defined(SHOW_NUMERIC_STRINGS) && !defined(SHOW_NUMERIC_CHARS)
#  define SHOW_NUMERIC_CHARS
#endif

  void print(char c) noexcept {
#ifndef SHOW_NUMERIC_CHARS
    os << '\'' << c << '\'';
#else
    os << "(char)" << static_cast<int>(c);
#endif
  }

private:
  template <typename String>
  void print_string(const String& str) noexcept {
#ifndef SHOW_NUMERIC_STRINGS
    os << '\"' << str << '\"';
#else
    print(make_range(str));
#endif
  }

public:
  void print(const std::string& str) noexcept { print_string(str); }

  void print(std::string_view sv) noexcept { print_string(sv); }

  // For cstrings. Nothing has to be done
  void print(const char* c) noexcept {
    if (!c) {
      print(nullptr);
    }
    else {
#ifndef SHOW_NUMERIC_STRINGS
      os << '\"' << c << '\"';
#else
      print(cstring_view(c));
#endif
    }
  }

  void print(const std::ostringstream& oss) noexcept {
    os << "\"" << oss.rdbuf() << "\"";
  }

  template <typename T, std::size_t N>
  void print(const T (&arr)[N]) noexcept {
    print(make_range(arr));
  }

private:
  struct range_printer {
    printer* _this;
    bool first_elem = true;
    template <typename T>
    void operator()(T&& x) noexcept {
      if (!first_elem) {
        _this->os << ", ";
      }
      first_elem = false;
      _this->print(x);
    }
  };

public:
  SHOW_TEMPLATE(typename Tuple)
  SHOW_REQUIRES(concepts::Tuple<Tuple>)
  void print(const Tuple& tpl) noexcept {
    os << "(";
    tuple_for_each(tpl, range_printer{this});
    os << ")";
  }

  void print(bool b) noexcept {
#ifndef SHOW_NO_BOOL_ALPHA
    auto _ = at_exit([this, flags = os.flags()] { os.setf(flags); });
    os << std::boolalpha;
#endif
    os << b;
  }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::OutputStreamable<T> && !concepts::Dereferencable<T> &&
                !std::is_enum<T>::value)
  void print(const T& x) noexcept { os << x; }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::Dereferencable<T> && !concepts::Wrapper<T>)
  void print(const T& x) noexcept {
    if (is_nullptr(x)) {
      print(nullptr);
    }
    else {
      print(*x);
    }
  }

  void print(std::nullptr_t) noexcept { os << "nullptr"; }

#ifndef SHOW_RANGE_LENGTH_LIMIT
#  define SHOW_RANGE_LENGTH_LIMIT 256
#endif

  SHOW_TEMPLATE(typename Rng)
  SHOW_REQUIRES(concepts::Range<Rng> && !concepts::OutputStreamable<Rng>)
  void print(const Rng& rng) noexcept {
    constexpr std::ptrdiff_t limit = SHOW_RANGE_LENGTH_LIMIT;

    os << '[';

    if constexpr (limit > 0) {
      auto last = for_each_n(rng, limit, range_printer{this});
      using std::end;
      if (last != end(rng)) {
        os << ", ...";
      }
    }
    else {
      for_each(rng, range_printer{this});
    }
    os << ']';
  }

  template <typename T, typename C>
  void print(const std::queue<T, C>& q) noexcept {
    print(detail::underlying_container(q));
  }

  template <typename T, typename C>
  void print(const std::stack<T, C>& s) noexcept {
    print(detail::underlying_container(s));
  }

  template <typename T, typename C, typename Comp>
  void print(const std::priority_queue<T, C, Comp>& pq) noexcept {
    print(detail::underlying_container(pq));
  }

  SHOW_TEMPLATE(typename E)
  SHOW_REQUIRES(std::is_enum<E>::value)
  void print(const E& e) noexcept {
    os << show_typename(e) << '('
       << static_cast<typename std::underlying_type<E>::type>(e) << ')';
  }

  SHOW_TEMPLATE(typename S)
  SHOW_REQUIRES(concepts::Struct<S> && !concepts::OutputStreamable<S> &&
                !concepts::Wrapper<S>)
  void print(const S& s) {
    os << show_typename(s) << '{';
    tuple_for_each(to_tuple(s), range_printer{this});
    os << '}';
  }

  SHOW_TEMPLATE(typename T)
  SHOW_REQUIRES(concepts::Wrapper<T>)
  void print(const T& x) {
    if (!has_value(x)) {
      os << "nullopt";
      return;
    }
    print(x.value());
  }

#if SHOW_BOOST_VARIANT
private:
  struct print_visitor : public boost::static_visitor<> {
    printer& p;
    print_visitor(printer& p_) noexcept : p(p_) {}
    template <typename T>
    void operator()(T&& x) noexcept {
      p.print(x);
    }
  };

public:
  template <typename... Ts>
  void print(const boost::variant<Ts...>& v) noexcept {
    print_visitor sv{*this};
    boost::apply_visitor(sv, v);
  }
#endif

  template <typename... Ts>
  void print(const std::variant<Ts...>& v) noexcept {
    std::visit([this](auto&& x) noexcept { return this->print(x); }, v);
  }
};
}} // namespace show::detail



namespace show { namespace detail {
template <typename T>
struct is_tuple_of_crefs : std::false_type {};
template <typename... Ts>
struct is_tuple_of_crefs<std::tuple<const Ts&...>> : std::true_type {};

/**
 * Proxy class to delay showing the requested value as a string.
 *
 * It prevents creating a temporary string using a stringstream when that string
 * has to printed to a stream anyway. In that case the value is directly shown
 * in the stream without creating a temporary.
 */
template <typename T>
class show_proxy {
  // Otherwise std::tuple<const Ts&...>& would lead to a dangling reference in
  // the variadic case.
  using U = std::decay_t<T>;
  using stored_type =
      std::conditional_t<is_tuple_of_crefs<U>::value, U, const T&>;
  stored_type _val;

public:
  explicit show_proxy(const stored_type& x) noexcept : _val(x) {}
  operator std::string() && noexcept {
    std::ostringstream oss;
    printer(oss).print(_val);
    return std::move(oss).str();
  }
  operator std::string() const& noexcept {
    SHOW_COMPILE_FAILURE(T,
                         "The result of show should not be captured with auto");
    return "";
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  show_proxy<T>&& proxy) noexcept {
    printer(os).print(proxy._val);
    return os;
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  const show_proxy<T>&) noexcept {
    SHOW_COMPILE_FAILURE(T,
                         "The result of show should not be captured with auto");
    return os;
  }
};
}} // namespace show::detail



namespace show {

template <typename T>
detail::show_proxy<T> show(const T& x) noexcept {
  return detail::show_proxy<T>{x};
}

SHOW_TEMPLATE(typename... Ts)
SHOW_REQUIRES(sizeof...(Ts) >= 2)
detail::show_proxy<std::tuple<const Ts&...>> show(const Ts&... xs) noexcept {
  return show(std::tie(xs...));
}

// Eager version of show
template <typename T>
std::string to_string(const T& x) noexcept {
  return static_cast<std::string>(show(x));
}

} // namespace show
#endif // SINGLE_HEADER_SHOW_HH
