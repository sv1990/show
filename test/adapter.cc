#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <queue>
#include <stack>

#include <doctest/doctest.h>

TEST_CASE("queue") {
  std::queue<int> q;
  q.push(1);
  q.push(2);
  q.push(3);
  CHECK(static_cast<std::string>(show::show(q)) == "[1, 2, 3]");
}

TEST_CASE("stack") {
  std::stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);
  CHECK(static_cast<std::string>(show::show(s)) == "[1, 2, 3]");
}

TEST_CASE("priority queue") {
  std::priority_queue<int> pq;
  pq.push(1);
  pq.push(1);
  pq.push(1);
  CHECK(static_cast<std::string>(show::show(pq)) == "[1, 1, 1]");
}
