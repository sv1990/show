#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <array>

#include <doctest/doctest.h>

TEST_CASE("simple") {
  int arr[] = {1, 2, 3};
  CHECK(static_cast<std::string>(show::show(arr)) == "[1, 2, 3]");
}

TEST_CASE("std_array") {
  std::array<int, 3> arr = {1, 2, 3};
  CHECK(static_cast<std::string>(show::show(arr)) == "[1, 2, 3]");
}
