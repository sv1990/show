#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#ifdef NDEBUG
#  undef NDEBUG
#endif

#include "contains.hh"
#include "redirect_stream.hh"

#define SHOW_ASSERT_NOABORT
#include "show/assert.hh"

#include <iostream>
#include <sstream>

#include <doctest/doctest.h>

TEST_CASE("concat") {
  CHECK(show::detail::concat(1, 2) == "12");
}

TEST_CASE("assert_print") {
  int a  = 2;
  auto s = show::detail::assert_print("a", a);
  CHECK(s == "a = 2\n");
}

TEST_CASE("failing") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  int a = 3, b = 2;
  ASSERT_LE(a, b);

  CHECK((contains(oss.str(), "Assertion 'a < b' failed with\na = 3\nb = 2")));
}

TEST_CASE("failing with literal 2") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  int a = 3;
  ASSERT_LE(a, 2);

  CHECK((contains(oss.str(), "Assertion 'a < 2' failed with\na = 3")));
}

TEST_CASE("failing with literal 3") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  int a = 2;
  ASSERT_LE(3, a);

  CHECK((contains(oss.str(), "Assertion '3 < a' failed with\na = 2")));
}

TEST_CASE("not failing") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  int a = 2, b = 3;
  ASSERT_LE(a, b);

  CHECK(oss.str() == "");
}

TEST_CASE("not failing") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  std::vector<int> a{1, 2, 3}, b{1, 2, 4};
  ASSERT_EQ(a, b);

  CHECK((contains(
      oss.str(),
      "Assertion 'a == b' failed with\na = [1, 2, 3]\nb = [1, 2, 4]")));
}

TEST_CASE("in_range1") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  ASSERT_IN_RANGE(1, 0.5, 1.5);
  ASSERT_IN_RANGE(1.9, 0, 1.5);

  CHECK((contains(oss.str(), "Assertion '0 <= 1.9 < 1.5' failed with")));
}

TEST_CASE("in_range2") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  std::vector<int> a{1, 2, 3};
  ASSERT_IN_RANGE(2, 0, a.size());
  ASSERT_IN_RANGE(5, 0, a.size());

  CHECK((contains(oss.str(),
                  "Assertion '0 <= 5 < a.size()' failed with\na.size() = 3")));
}

TEST_CASE("floating") {
  std::ostringstream oss;
  auto _ = redirect_stream{std::cerr, oss};

  double a = 0.1, b = 0.2;

  ASSERT_EQ(a + b, 0.3);

  CHECK(oss.str() == "");
}
