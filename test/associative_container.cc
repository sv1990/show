#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"
#include "show/util.hh"

#include <doctest/doctest.h>

#include <map>

TEST_CASE("simple") {
  std::multimap<int, int> mmii;
  mmii.emplace(1, 2);
  mmii.emplace(1, 3);
  mmii.emplace(2, 5);
  mmii.emplace(3, 8);
  mmii.emplace(1, 5);
  CHECK(static_cast<std::string>(show::show(show::make_range(mmii))) ==
        "[(1, 2), (1, 3), (1, 5), (2, 5), (3, 8)]");
}

TEST_CASE("equal_range") {
  std::multimap<int, int> mmii;
  mmii.emplace(1, 2);
  mmii.emplace(1, 3);
  mmii.emplace(2, 5);
  mmii.emplace(3, 8);
  mmii.emplace(1, 5);
  auto er = mmii.equal_range(1);
  // Interpret pairs of iterators as ranges
  CHECK(static_cast<std::string>(show::show(show::make_range(er))) ==
        "[(1, 2), (1, 3), (1, 5)]");
}
