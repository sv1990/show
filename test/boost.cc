#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

#if __has_include(<boost/optional.hpp>)
#  include <boost/optional.hpp>

TEST_CASE("optional") {
  boost::optional<int> o;
  CHECK(static_cast<std::string>(show::show(o)) == "nullopt");
  o = 2;
  CHECK(static_cast<std::string>(show::show(o)) == "2");
}
#endif

#if SHOW_BOOST_VARIANT
TEST_CASE("variant") {
  boost::variant<int, std::string> v = 1;
  CHECK(static_cast<std::string>(show::show(v)) == "1");
  v = "abc";
  CHECK(static_cast<std::string>(show::show(v)) == "\"abc\"");
}
#endif
