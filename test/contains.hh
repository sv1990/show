#ifndef SHOW_TEST_CONTAINS_HH_1556521526326550313_
#define SHOW_TEST_CONTAINS_HH_1556521526326550313_

#include <string>

inline bool contains(const std::string& str,
                     const std::string& substr) noexcept {
  return str.find(substr) != std::string::npos;
}

#endif // SHOW_TEST_CONTAINS_HH_1556521526326550313_
