#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

#include <optional>
#include <string_view>
#include <variant>

TEST_CASE("optional") {
  std::optional<int> o;
  CHECK(static_cast<std::string>(show::show(o)) == "nullopt");
  o = 2;
  CHECK(static_cast<std::string>(show::show(o)) == "2");
}

TEST_CASE("string_view") {
  std::string_view s = "abc";
  CHECK(static_cast<std::string>(show::show(s)) == "\"abc\"");
}

TEST_CASE("variant") {
  std::variant<int, std::string> v = 1;
  CHECK(static_cast<std::string>(show::show(v)) == "1");
  v = "abc";
  CHECK(static_cast<std::string>(show::show(v)) == "\"abc\"");
}
