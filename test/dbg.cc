#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "contains.hh"
#include "redirect_stream.hh"

#include "show/dbg.hh"

#include <sstream>
#include <vector>

#include <doctest/doctest.h>

TEST_CASE("dbg") {
  SUBCASE("simple") {
    std::ostringstream oss;
    redirect_stream rs{std::clog, oss};

    std::vector<int> n = {1, 2, 3};

    DBG(n);

    CHECK(contains(oss.str(), "dbg.cc(20): n = [1, 2, 3]\n"));
  }
}
