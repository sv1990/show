#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

#include <tuple>

struct derived_tuple : std::tuple<int, double, std::string> {
  derived_tuple(int i, double d, const std::string& s)
      : std::tuple<int, double, std::string>(i, d, s) {}
};

TEST_CASE("derived_tuple") {
  derived_tuple d(1, 2.5, "Blah");
  CHECK(static_cast<std::string>(show::show(d)) == "(1, 2.5, \"Blah\")");
}

struct derived_pair : std::pair<int, double> {
  derived_pair(int i, double d) : std::pair<int, double>(i, d) {}
};

TEST_CASE("derived_pair") {
  derived_pair d(1, 2.5);
  CHECK(static_cast<std::string>(show::show(d)) == "(1, 2.5)");
}
