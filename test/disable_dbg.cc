#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <doctest/doctest.h>

#define SHOW_DISABLE_TEE
#include "show/dbg.hh"

#include <sstream>

TEST_CASE("dbg") {
  std::ostringstream oss;
  auto buf = std::cout.rdbuf(oss.rdbuf());

  int n = 42;
  SHOW_DBG(n);
  CHECK(oss.str() == "");
  std::cout.rdbuf(buf);
}
