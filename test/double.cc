#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

TEST_CASE("simple") {
  double x = 1;
  CHECK(static_cast<std::string>(show::show(x)) == "1");
  x = 1e-8;
  CHECK(static_cast<std::string>(show::show(x)) == "1e-08");
  x = 0.00000001;
  CHECK(static_cast<std::string>(show::show(x)) == "1e-08");
}
