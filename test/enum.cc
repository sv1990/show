#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#define SHOW_SILENT_UNPRINTABLE
#include "show/show.hh"

#include <doctest/doctest.h>

enum class blah { foo, bar = 5, baz };

TEST_CASE("simple") {
  CHECK(show::to_string(blah::foo) == "blah(0)");
  CHECK(show::to_string(blah::bar) == "blah(5)");
  CHECK(show::to_string(blah::baz) == "blah(6)");
}

enum plain { FOO, BAR = 5, BAZ };

TEST_CASE("plain") {
  CHECK(show::to_string(FOO) == "plain(0)");
  CHECK(show::to_string(BAR) == "plain(5)");
  CHECK(show::to_string(BAZ) == "plain(6)");
}
