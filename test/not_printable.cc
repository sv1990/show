#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

class not_printable_t {
  int x;

public:
  not_printable_t() { (void)x; }
};

TEST_CASE("simple") {
  CHECK(static_cast<std::string>(show::show(not_printable_t{})) ==
        "{show: not printable}");
}
