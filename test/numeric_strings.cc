#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#define SHOW_NUMERIC_STRINGS
#include "show/show.hh"

#include <string>

#include <doctest/doctest.h>

TEST_CASE("numeric chars") {
  char c = 100;
  CHECK(static_cast<std::string>(show::show(c)) == "(char)100");
}

TEST_CASE("numeric strings") {
  std::string s = "hallo";
  CHECK(static_cast<std::string>(show::show(s)) ==
        "[(char)104, (char)97, (char)108, (char)108, (char)111]");
  const char* c = "hallo";
  CHECK(static_cast<std::string>(show::show(c)) ==
        "[(char)104, (char)97, (char)108, (char)108, (char)111]");
}
