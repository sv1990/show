#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

TEST_CASE("pointer") {
  int x = 42;
  CHECK(static_cast<std::string>(show::show(&x)) == "42");
  int* p = nullptr;
  CHECK(static_cast<std::string>(show::show(p)) == "nullptr");
}

TEST_CASE("smart_pointer") {
  auto up = std::make_unique<int>(42);
  CHECK(static_cast<std::string>(show::show(up)) == "42");
  auto p = std::unique_ptr<int>();
  CHECK((static_cast<std::string>(show::show(p)) == "nullptr"));
}

TEST_CASE("iterator") {
  auto v = std::vector<int>{42};
  CHECK(static_cast<std::string>(show::show(begin(v))) == "42");
}
