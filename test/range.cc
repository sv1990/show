#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

#include <list>
#include <vector>

TEST_CASE("empty") {
  std::vector<int> v{};
  CHECK(static_cast<std::string>(show::show(v)) == "[]");
}

TEST_CASE("simple") {
  std::vector<int> v{1, 2, 3};
  CHECK(static_cast<std::string>(show::show(v)) == "[1, 2, 3]");
}

TEST_CASE("nested") {
  std::vector<std::list<int>> v{{1}, {2, 3}, {4, 5, 6}};
  CHECK(static_cast<std::string>(show::show(v)) == "[[1], [2, 3], [4, 5, 6]]");
}
