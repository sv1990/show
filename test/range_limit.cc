#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#define SHOW_RANGE_LENGTH_LIMIT 5
#include "show/show.hh"

#include <doctest/doctest.h>

#include <vector>

TEST_CASE("simple") {
  std::vector<int> v{1, 2, 3, 4, 5};
  CHECK(static_cast<std::string>(show::show(v)) == "[1, 2, 3, 4, 5]");
  v.push_back(6);
  CHECK(static_cast<std::string>(show::show(v)) == "[1, 2, 3, 4, 5, ...]");
}
