#ifndef SHOW_TEST_REDIRECT_STREAM_HH_1533561368784491120_
#define SHOW_TEST_REDIRECT_STREAM_HH_1533561368784491120_

#include <ostream>

class redirect_stream {
  std::ostream* _from;
  std::streambuf* _buf;

public:
  redirect_stream(std::ostream& from, std::ostream& to)
      : _from(&from), _buf(_from->rdbuf(to.rdbuf())) {}
  ~redirect_stream() { _from->rdbuf(_buf); }
};

#endif // SHOW_TEST_REDIRECT_STREAM_HH_1533561368784491120_
