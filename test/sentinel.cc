#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

TEST_CASE("cstring") {
  show::detail::cstring_view cstr("abc");
  // Works also with iterator-sentinel-pairs as ranges
  CHECK(static_cast<std::string>(show::show(cstr)) == "['a', 'b', 'c']");
}
