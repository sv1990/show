#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

TEST_CASE("std_string") {
  std::string str = "abc";
  CHECK(static_cast<std::string>(show::show(str)) == "\"abc\"");
}

TEST_CASE("c_string") {
  const char* cstr = "abc";
  CHECK(static_cast<std::string>(show::show(cstr)) == "\"abc\"");
  cstr = nullptr;
  CHECK(static_cast<std::string>(show::show(cstr)) == "nullptr");
}

TEST_CASE("static_string") {
  const char cstr[] = "abc";
  CHECK(static_cast<std::string>(show::show(cstr)) == "\"abc\"");
}
