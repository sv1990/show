#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#define SHOW_SILENT_UNPRINTABLE
#include "show/show.hh"

#include <doctest/doctest.h>

#include <memory>

struct S {
  int a, b, c;
};

TEST_CASE("simple") {
  S s{1, 2, 3};
#if __cplusplus >= 201703L
  CHECK(static_cast<std::string>(show::show(s)) == "S{1, 2, 3}");
#else
  CHECK(static_cast<std::string>(show::show(s)) == "{show: not printable}");
#endif
}

struct move_only {
  std::unique_ptr<int> x;
};

TEST_CASE("move_only") {
  move_only m{std::make_unique<int>(42)};
#if __cplusplus >= 201703L
  CHECK(static_cast<std::string>(show::show(m)) == "move_only{42}");
#else
  CHECK(static_cast<std::string>(show::show(m)) == "{show: not printable}");
#endif
}

struct node {
  int value;
  std::unique_ptr<node> next = nullptr;
};

TEST_CASE("linked_list") {
  node n{1, std::unique_ptr<node>(
                new node{2, std::unique_ptr<node>(new node{3})})};
#if __cplusplus >= 201703L
  CHECK(static_cast<std::string>(show::show(n)) ==
        "node{1, node{2, node{3, nullptr}}}");
#else
  CHECK(static_cast<std::string>(show::show(n)) == "{show: not printable}");
#endif
}
