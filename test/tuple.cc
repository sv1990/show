#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"
#include "show/util.hh"

#include <doctest/doctest.h>

TEST_CASE("pair") {
  std::pair<int, int> p{1, 2};
  CHECK(static_cast<std::string>(show::show(p)) == "(1, 2)");
}

TEST_CASE("tuple") {
  std::tuple<int, int, int> t{1, 2, 3};
  CHECK(static_cast<std::string>(show::show(t)) == "(1, 2, 3)");
}

TEST_CASE("empty tuple") {
  std::tuple<> t;
  CHECK(static_cast<std::string>(show::show(t)) == "()");
}

TEST_CASE("variadic") {
  int x = 1, y = 2, z = 3;
  CHECK(static_cast<std::string>(show::show(x, y, z)) == "(1, 2, 3)");
  CHECK(static_cast<std::string>(show::show(1, 2, 3)) == "(1, 2, 3)");
}
