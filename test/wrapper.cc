#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "show/show.hh"

#include <doctest/doctest.h>

#include <memory>

template <typename T>
class wrapper {
  T _x;

public:
  explicit wrapper(T x) : _x(std::move(x)) {}
  const T& value() const { return _x; }
};

TEST_CASE("wrapper") {
  wrapper<int> w(42);
  CHECK(show::to_string(w) == "42");
}

template <typename T>
class optional {
  std::unique_ptr<T> _x;

public:
  explicit optional() : _x(nullptr) {}
  explicit optional(T x) : _x(std::make_unique<T>(x)) {}
  const T& value() const { return *_x; }
  bool has_value() const { return static_cast<bool>(_x); }
};

TEST_CASE("optional") {
  optional<int> o1;
  CHECK(show::to_string(o1) == "nullopt");
  optional<int> o2(42);
  auto s = show::to_string(o2);
  CHECK(show::to_string(o2) == "42");
}

template <std::size_t N>
struct wrapper2 {
  std::size_t value() const noexcept { return N; }
};

TEST_CASE("wrapper2") {
  wrapper2<42> w;
  CHECK(show::to_string(w) == "42");
}
